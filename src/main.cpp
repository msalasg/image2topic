/*	
 * Project: Images2Topic
 * Author: Marta Salas 
 * Date: March 2014
 * Description: reads sequences of presyncronized rgbd and publish 
 * them on ROS topics
 *
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/String.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <std_msgs/Bool.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/transforms.h>

#include <geometry_msgs/Point.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <cmath>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <limits>

#include <unistd.h>

#include "DUtils.h"

ros::Publisher m_image_color_pub;
ros::Publisher m_image_depth_pub;
ros::Publisher save_pcl_pub;
ros::Subscriber start_sub;
bool bstart; 
  
void start_play(const std_msgs::Bool::ConstPtr& msg);

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "Images2Topic");
  
  ros::NodeHandle node;
  
  m_image_color_pub = node.advertise<sensor_msgs::Image>
    ("/camera/image_color", 100);
 
  m_image_depth_pub = node.advertise<sensor_msgs::Image>
    ("/camera/image_depth", 100);
  
  save_pcl_pub = node.advertise<std_msgs::Bool>
    ("/vslam/save_pcl", 100);
  

  bstart = false; 
  
  start_sub =  node.subscribe("/Image2Topic/start", 1, &start_play);

  std::string images_path;
  std::string depth_path;
  std::string images_extension;
  std::string depth_extension;
  double ftp; 

  ros::param::param<std::string>("~images_path",images_path,"");
  ros::param::param<std::string>("~depth_path",depth_path,"");
  ros::param::param<std::string>("~images_extension",images_extension,".jpg");
  ros::param::param<std::string>("~depth_extension",depth_extension,".png");
  ros::param::param<double>("~ftps",ftp,30); 
    
  if(!DUtils::FileFunctions::DirExists(images_path.c_str()))
  {
    ROS_WARN("Could not read images from directory %s", images_path.c_str());
    return 0; 
  }

  std::vector<std::string> filenames_color;
  std::vector<std::string> filenames_depth;
  
  std::vector<std::string> f = 
      DUtils::FileFunctions::Dir(images_path.c_str(), images_extension.c_str(), 
        false, false); // false, false == no sort, no case sensitive
    
  filenames_color.insert(filenames_color.end(), f.begin(), f.end());
  std::sort(filenames_color.begin(), filenames_color.end());
 
 f = DUtils::FileFunctions::Dir(depth_path.c_str(), depth_extension.c_str(), 
        false, false); // false, false == no sort, no case sensitive
    
  filenames_depth.insert(filenames_depth.end(), f.begin(), f.end());
    
  std::sort(filenames_depth.begin(), filenames_depth.end());
  
  if(filenames_depth.size()!=filenames_color.size()){
    ROS_WARN("The number of color images not match with the number of depth images");
  }

  // publish images
  DUtils::TimeManager timeManager_color; 
  DUtils::TimeManager timeManager_depth; 
  DUtils::Timestamp timestamp_color; 
  DUtils::Timestamp timestamp_depth; 
  for(size_t i = 0; i < filenames_color.size(); ++i)
  {
    std::string stimestamp_color = filenames_color[i]; 
    stimestamp_color.erase(stimestamp_color.find_last_of("-"), string::npos);
    stimestamp_color.erase(0, stimestamp_color.find("-")+1);
    timestamp_color.setTime(stimestamp_color);
    timeManager_color.add(timestamp_color); 
  }
  for(size_t i = 0; i < filenames_depth.size(); ++i)
  {
    std::string stimestamp_depth = filenames_depth[i]; 
    stimestamp_depth.erase(stimestamp_depth.find_last_of("-"), string::npos);
    stimestamp_depth.erase(0, stimestamp_depth.find("-")+1);
    timestamp_depth.setTime(stimestamp_depth);
    timeManager_depth.add(timestamp_depth); 
   }
   DUtils::TimeManager::iterator tit1, tit2;
   tit1 = timeManager_color.begin();
   tit2 = timeManager_depth.begin(); 
   
   //float frequency = 33; 
   //tit1 = timeManager_color.begin(frequency); 
   //tit2 = timeManager_depth.begin(frequency); 
   //if(tit1.timestamp.getFloatTime() > tit2.timestamp.getFloatTime()) 
   //   tit2 = timeManager_depth.beginAt(tit1.timestamp,frequency);
   //else
   //   tit1 = timeManager_color.beginAt(tit2.timestamp,frequency);
   
   ROS_INFO("%d images on this sequence",filenames_color.size());
   //ROS_INFO("Press ENTER to start publishing");
   //std::cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
   while(bstart==false)  ros::spinOnce();
   sleep(5);
   ros::Rate rate(ftp);
   //for(; tit1.good() && tit2.good(); ++tit1, ++tit2)
   //for(; tit1.good() ; ++tit1)
   for(; tit1.good() && tit2.good(); ++tit1, ++tit2)
   {

    cv::Mat im_color = cv::imread(filenames_color[tit1.index]);
    cv::Mat im_depth_uint16 = cv::imread(filenames_depth[tit2.index],CV_LOAD_IMAGE_ANYDEPTH);
    cv::Mat im_depth;
    cv::Mat mask;
    cv::compare(im_depth_uint16,cv::Mat::zeros(im_depth_uint16.rows, im_depth_uint16.cols, CV_16U), mask, cv::CMP_EQ);
    im_depth_uint16.convertTo(im_depth,CV_32F); 
    im_depth.setTo(NAN,mask);
    
    im_depth = im_depth/1000;
    
    if(im_color.empty())
    {
      ROS_WARN("Image %s , timestamp %f", filenames_color[tit1.index].c_str(), tit1.timestamp.getFloatTime());
    }
    else if(im_depth.empty())
    {
      ROS_WARN("Image %s , timestamp %f", filenames_depth[tit2.index].c_str(),tit2.timestamp.getFloatTime());
    }
    else
    {
      cv_bridge::CvImage cvi_color;
      cvi_color.header.stamp = ros::Time(tit1.timestamp.getFloatTime()); 
      cvi_color.header.frame_id = "0";
      cvi_color.encoding = "bgr8";
      cvi_color.image = im_color;

      sensor_msgs::Image im_msg_color;
      cvi_color.toImageMsg(im_msg_color);
      m_image_color_pub.publish(im_msg_color);

      cv_bridge::CvImage cvi_depth;
      cvi_depth.header.stamp = ros::Time(tit2.timestamp.getFloatTime()); //cvi_color.header.stamp;
      cvi_depth.header.frame_id = "0";

      cvi_depth.encoding = "32FC1";
      cvi_depth.image = im_depth;

      sensor_msgs::Image im_msg_depth;
      cvi_depth.toImageMsg(im_msg_depth);
      m_image_depth_pub.publish(im_msg_depth);
       

      //tit2 = timeManager_depth.beginAt(tit1.timestamp,frequency);
      rate.sleep();
    }
    
  }
  ROS_WARN("I finish the for loop");
  ROS_WARN("Size timeManager_color %d", timeManager_color.size());
  ROS_WARN("Size color %d", filenames_color.size());
  ROS_WARN("Size timeManager_depth %i", timeManager_depth.size());
  ROS_WARN("Size depths %i", filenames_depth.size());
  
  std_msgs::Bool savePCL;
  savePCL.data = true; 
  save_pcl_pub.publish(savePCL);
  ros::spin();

  return 0;
}

void start_play(const std_msgs::Bool::ConstPtr& msg)
{
  bstart = true; 
}


