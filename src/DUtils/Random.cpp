/*	
 * File: Random.cpp
 * Project: DUtils library
 * Author: Dorian Galvez-Lopez
 * Date: April 2010
 * Description: manages pseudo-random numbers
 *
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Random.h"
#include "Timestamp.h"
#include "STL.h"
#include <algorithm>
#include <numeric>
#include <cstdlib>
using namespace std;

bool DUtils::Random::m_already_seeded = false;

void DUtils::Random::SeedRand(){
	Timestamp time;
	time.setToCurrentTime();
	srand((unsigned)time.getFloatTime()); 
}

void DUtils::Random::SeedRandOnce()
{
  if(!m_already_seeded)
  {
    DUtils::Random::SeedRand();
    m_already_seeded = true;
  }
}

void DUtils::Random::SeedRand(int seed)
{
	srand(seed); 
}

void DUtils::Random::SeedRandOnce(int seed)
{
  if(!m_already_seeded)
  {
    DUtils::Random::SeedRand(seed);
    m_already_seeded = true;
  }
}

int DUtils::Random::RandomInt(int min, int max){
	int d = max - min + 1;
	return int(((double)rand()/((double)RAND_MAX + 1.0)) * d) + min;
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

DUtils::Random::UnrepeatedRandomizer::UnrepeatedRandomizer(int min, int max)
{
  if(min <= max)
  {
    m_min = min;
    m_max = max;
  }
  else
  {
    m_min = max;
    m_max = min;
  }

  createValues();
}

// ---------------------------------------------------------------------------

DUtils::Random::UnrepeatedRandomizer::UnrepeatedRandomizer
  (const DUtils::Random::UnrepeatedRandomizer& rnd)
{
  *this = rnd;
}

// ---------------------------------------------------------------------------

int DUtils::Random::UnrepeatedRandomizer::get()
{
  if(empty()) createValues();
  
  DUtils::Random::SeedRandOnce();
  
  int k = DUtils::Random::RandomInt(0, m_values.size()-1);
  int ret = m_values[k];
  m_values[k] = m_values.back();
  m_values.pop_back();
  
  return ret;
}

// ---------------------------------------------------------------------------

void DUtils::Random::UnrepeatedRandomizer::createValues()
{
  int n = m_max - m_min + 1;
  
  m_values.resize(n);
  for(int i = 0; i < n; ++i) m_values[i] = m_min + i;
}

// ---------------------------------------------------------------------------

void DUtils::Random::UnrepeatedRandomizer::reset()
{
  if((int)m_values.size() != m_max - m_min + 1) createValues();
}

// ---------------------------------------------------------------------------

DUtils::Random::UnrepeatedRandomizer& 
DUtils::Random::UnrepeatedRandomizer::operator=
  (const DUtils::Random::UnrepeatedRandomizer& rnd)
{
  if(this != &rnd)
  {
    this->m_min = rnd.m_min;
    this->m_max = rnd.m_max;
    this->m_values = rnd.m_values;
  }
  return *this;
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

DUtils::Random::UnrepeatedWeightedRandomizer::UnrepeatedWeightedRandomizer
  (const std::vector<double> &weights, int min)
  : m_min(min), m_weights(weights)
{
  createValues();
}

// ---------------------------------------------------------------------------

void DUtils::Random::UnrepeatedWeightedRandomizer::init
  (const std::vector<double> &weights, int min)
{
  m_min = min;
  m_weights = weights;
  createValues();
}

// ---------------------------------------------------------------------------

void DUtils::Random::UnrepeatedWeightedRandomizer::createValues()
{
  vector<unsigned int> i_sort;
  STL::indexSort(m_weights.begin(), m_weights.end(), i_sort); // ascending
  
  vector<double> test = m_weights;
  STL::arrange(test.begin(), test.end(), i_sort);
  
  m_current_weights.clear();
  m_current_weights.reserve(m_weights.size());
  
  m_range.clear();
  m_range.reserve(m_weights.size());
  
  vector<unsigned int>::const_iterator iit;
  for(iit = i_sort.begin(); iit != i_sort.end(); ++iit)
  {
    m_current_weights.push_back(m_weights[*iit]);
    m_range.push_back(m_min + *iit);
  }  
  
  m_sum = std::accumulate(m_weights.begin(), m_weights.end(), 0.);
}

// ---------------------------------------------------------------------------

void DUtils::Random::UnrepeatedWeightedRandomizer::reset()
{
  if(m_current_weights.size() < m_weights.size()) createValues();
}

// ---------------------------------------------------------------------------

int DUtils::Random::UnrepeatedWeightedRandomizer::get()
{
  if(empty()) createValues();
  
  DUtils::Random::SeedRandOnce();
  
  double cut = RandomValue<double>(0, m_sum);
  
  int i = ((int)m_current_weights.size()) - 1; 
  double s = m_sum - m_current_weights.back();
  
  while(cut <= s && i > 0)
  {
    --i;
    s -= m_current_weights[i];
  }
  
  // selected number from the range
  int k = m_range[i];
  
  // remove the i-th element now, keeping the weights in ascending order
  m_sum -= m_current_weights[i];
  
  std::copy(m_current_weights.begin() + i + 1, m_current_weights.end(),
    m_current_weights.begin() + i);
  std::copy(m_range.begin() + i + 1, m_range.end(),
    m_range.begin() + i);
  
  m_current_weights.pop_back();
  m_range.pop_back();
  
  return k;  
}

// ---------------------------------------------------------------------------

